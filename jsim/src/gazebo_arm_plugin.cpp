// Beginnings of a gazebo plugin for Jaco arm
// Based on emu plugin by Germán Castro

#include <map>

#include <gazebo/common/Plugin.hh>
#include <gazebo/common/common.hh>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>

#include <ros/ros.h>

#include <sensor_msgs/JointState.h>

#define KP 100.0
#define KD 10.0
#define KI 0.01

namespace gazebo {

    class JsimGazeboPlugin : public ModelPlugin {
    private:
        physics::ModelPtr model;
        ros::Subscriber jointSub;
        std::map<std::string, double> lastErrorMap;
        std::map<std::string, double> sumErrorMap;

    public:

        JsimGazeboPlugin() : ModelPlugin() {
            ROS_INFO("Creating JsimGazeboPlugin.");
        }

        void Load(physics::ModelPtr _model, sdf::ElementPtr /*_sdf*/) {
            ROS_INFO("Initialising JsimGazeboPlugin.");

            if (!ros::isInitialized()) {
                ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load plugin. "
                        << "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
                return;
            }

            ros::NodeHandle node;
            jointSub = node.subscribe("joint_control", 1, &JsimGazeboPlugin::callbackJoints, this);

            model = _model;
        }

        void callbackJoints(sensor_msgs::JointStateConstPtr joints) {
            size_t jointCount = MIN(joints->name.size(), joints->position.size());

            for (size_t i = 0; i < jointCount; ++i) {

                if (joints->name[i] == "arm_0_joint" ||
                        joints->name[i] == "arm_1_joint" ||
                        joints->name[i] == "arm_2_joint" ||
                        joints->name[i] == "arm_3_joint" ||
                        joints->name[i] == "arm_4_joint" ||
                        joints->name[i] == "arm_5_joint") {
                    setJointPosition(joints->name[i], joints->position[i]);
                }
            }
        }

        void setJointPosition(std::string _jointName, float position) {
            std::string jointName = "jaco::" + _jointName;
            physics::JointPtr joint = model->GetJoint(jointName);
            int axis;

            joint->SetVelocity(0, 0.0);
            joint->SetVelocity(1, 0.0);
            joint->SetVelocity(2, 0.0);

            if (joint->GetVelocityLimit(0) != 0) {
                axis = 0;
            } else if (joint->GetVelocityLimit(1) != 0) {
                axis = 1;
            } else {
                axis = 2;
            }

            double velocity = PIDControl(jointName, joint->GetAngle(axis), math::Angle(position));

            velocity = MIN(velocity, abs(joint->GetVelocityLimit(axis)));
            velocity = MAX(velocity, -abs(joint->GetVelocityLimit(axis)));

            joint->SetMaxForce(axis, joint->GetEffortLimit(axis));
            model->SetJointPosition(jointName, position);
            joint->SetVelocity(axis, velocity);
            joint->Update();

            ROS_DEBUG("%s velocity %f (%f) position %f target %f", jointName.c_str(), joint->GetVelocity(axis),
                    joint->GetVelocityLimit(axis), joint->GetAngle(axis).Radian(), position);

        }

        double PIDControl(std::string jointName, math::Angle position, math::Angle target) {
            double error = (target - position).Radian();
            double lastError = getLastError(jointName);
            double sumError = getSumError(jointName);

            // generalized PID formula
            //correction = Kp * error + Kd * (error - prevError) + kI * (sum of errors)

            // calculate a motor speed for the current conditions
            double motorSpeed = KP * error;

            motorSpeed += KD * (error - lastError);
            motorSpeed += KI * (sumError);
            // set the last and sumerrors for next loop iteration
            lastError = error;
            sumError += error;

            ROS_DEBUG("speed %f error %f lastError %f sumError %f", motorSpeed, error, lastError, sumError);

            setLastError(jointName, lastError);
            setSumError(jointName, sumError);

            return motorSpeed;
        }

        double getLastError(std::string jointName) {
            double lastError;
            std::map<std::string, double>::iterator it;

            it = lastErrorMap.find(jointName);
            if (it == lastErrorMap.end()) {
                lastErrorMap.insert(std::pair<std::string, double>(jointName, 0.0));
                lastError = 0;
            } else {
                lastError = (*it).second;
            }
            return lastError;
        }

        void setLastError(std::string jointName, double error) {
            lastErrorMap.erase(jointName);
            lastErrorMap.insert(std::pair<std::string, double>(jointName, error));
        }

        double getSumError(std::string jointName) {
            double sumError;
            std::map<std::string, double>::iterator it;

            it = sumErrorMap.find(jointName);
            if (it == sumErrorMap.end()) {
                sumErrorMap.insert(std::pair<std::string, double>(jointName, 0.0));
                sumError = 0;
            } else {
                sumError = (*it).second;
            }
            return sumError;
        }

        void setSumError(std::string jointName, double error) {
            sumErrorMap.erase(jointName);
            sumErrorMap.insert(std::pair<std::string, double>(jointName, error));
        }

    };
    GZ_REGISTER_MODEL_PLUGIN(JsimGazeboPlugin);
}
