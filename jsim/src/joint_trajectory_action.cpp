// JACO FollowJointTrajectoryAction node
// This node takes a joint trajectory (set of joint angle waypoints) and 
// executes them using the ROS JACO package (Fuerte version)
//
// Ian Craig minicraig@gmail.com
// 03-11-2013

#include <ros/ros.h>
#include <actionlib/server/action_server.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>

#include <trajectory_msgs/JointTrajectory.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <control_msgs/FollowJointTrajectoryFeedback.h>
#include <sensor_msgs/JointState.h>

#include <jaco_driver/ArmJointAnglesAction.h>
#include <jaco_driver/SetFingersPositionAction.h>

using namespace ros;
const double GOAL_THRESHOLD = 1.0;

class JointTrajectoryExecuter {
private:
    typedef actionlib::ActionServer<control_msgs::FollowJointTrajectoryAction> JTAS;
    typedef JTAS::GoalHandle GoalHandle;
    typedef actionlib::SimpleActionClient<jaco_driver::ArmJointAnglesAction> JAAC;
    typedef actionlib::SimpleActionClient<jaco_driver::SetFingersPositionAction> JFAC;
public:

    JointTrajectoryExecuter(NodeHandle &n) :
    nh(n),
    action_server(nh, "jsim_arm_controller/follow_joint_trajectory_action",
    boost::bind(&JointTrajectoryExecuter::goal_callback, this, _1),
    boost::bind(&JointTrajectoryExecuter::cancel_callback, this, _1),
    false),
    driver_arm("jaco/arm_joint_angles", true),
    driver_fingers("jaco/finger_joint_angles", true),
    has_goal(false) {
        // Create list of joints for arm, fingers, all. This allows us to handle
        // incoming trajectories for each of these with the same controller
        // without sending blank messages to JACO.
        arm_joint_names.push_back("arm_0_joint");
        arm_joint_names.push_back("arm_1_joint");
        arm_joint_names.push_back("arm_2_joint");
        arm_joint_names.push_back("arm_3_joint");
        arm_joint_names.push_back("arm_4_joint");
        arm_joint_names.push_back("arm_5_joint");
        finger_joint_names.push_back("finger_joint_1");
        finger_joint_names.push_back("finger_joint_2");
        finger_joint_names.push_back("finger_joint_3");
        joint_names.insert(joint_names.end(), arm_joint_names.begin(), arm_joint_names.end());
        joint_names.insert(joint_names.end(), finger_joint_names.begin(), finger_joint_names.end());

        // Listen to joint_states so we can preempt the next waypoint
        sub_angles = nh.subscribe("joint_states", 1, &JointTrajectoryExecuter::state_callback, this);

        // Wait for JACO actions to be ready
        ROS_INFO("Waiting for jaco_driver to start");
        driver_arm.waitForServer();
        driver_fingers.waitForServer();

        // Set status to ready
        action_server.start();
        ROS_INFO("JACO Joint Trajectory Executer is running...");
    }

    ~JointTrajectoryExecuter() {
        sub_angles.shutdown();
        driver_arm.cancelAllGoals();
        driver_fingers.cancelAllGoals();
    }

private:

    NodeHandle nh;
    JTAS action_server;
    JAAC driver_arm;
    JFAC driver_fingers;
    Subscriber sub_angles;

    bool has_goal;
    GoalHandle goal;
    trajectory_msgs::JointTrajectory current_traj;
    int traj_stage, executed_stage;

    std::vector<std::string> joint_names, arm_joint_names, finger_joint_names, active_joints;
    sensor_msgs::JointState joint_states;

    /**
     * Check that two sets are equal (by subset of each other)
     * @param a
     * @param b
     * @return 
     */
    static bool setsEqual(const std::vector<std::string> &a, const std::vector<std::string> &b) {
        return (subset(a, b) && subset(b, a));
    }

    /**
     * Check that vector a is a subset of vector b
     * @param a
     * @param b
     * @return 
     */
    static bool subset(const std::vector<std::string> &a, const std::vector<std::string> &b) {
        if (a.size() > b.size())
            return false;

        for (size_t i = 0; i < a.size(); ++i) {
            if (count(b.begin(), b.end(), a[i]) != 1)
                return false;
        }

        return true;
    }

    /**
     * Compare joint angles to determine whether we are "close enough" to the
     * waypoint / goal.
     * @param joint_states
     * @param point
     * @return 
     */
    bool atPose(const sensor_msgs::JointState joint_states, const ::trajectory_msgs::JointTrajectoryPoint point) {
        ROS_DEBUG("Checking goal constraints");
        if (!joint_states.header.seq)
            return false;
        for (size_t i = 0; i < joint_names.size(); ++i) {
            double abs_error = fabs(joint_states.position[i] - point.positions[i]);
            ROS_DEBUG("abs_error: %f", abs_error);
            if (abs_error > GOAL_THRESHOLD)
                return false;
        }
        return true;
    }

    /**
     * Send next waypoint to JACO
     * Keeps a counter for which waypoint it has executed so that it doesn't
     * re-execute. Other functions can skip points by updating traj_stage.
     */
    void execute() {
        if (traj_stage <= executed_stage) return;
        executed_stage = traj_stage;

        ROS_INFO("Pushing action %d", traj_stage);

        int i = 0;
        if (active_joints == joint_names || active_joints == arm_joint_names) {
            jaco_driver::ArmJointAnglesActionGoal goal;
            goal.goal.angles.Angle_J1 = current_traj.points[traj_stage].positions[i++];
            goal.goal.angles.Angle_J2 = current_traj.points[traj_stage].positions[i++];
            goal.goal.angles.Angle_J3 = current_traj.points[traj_stage].positions[i++];
            goal.goal.angles.Angle_J4 = current_traj.points[traj_stage].positions[i++];
            goal.goal.angles.Angle_J5 = current_traj.points[traj_stage].positions[i++];
            goal.goal.angles.Angle_J6 = current_traj.points[traj_stage].positions[i++];
            driver_arm.sendGoal(goal.goal, boost::bind(&JointTrajectoryExecuter::driver_arm_done_callback, this, _1, _2));
        }
        if (active_joints == joint_names || active_joints == finger_joint_names) {
            jaco_driver::SetFingersPositionActionGoal goal;
            goal.goal.fingers.Finger_1 = current_traj.points[traj_stage].positions[i++];
            goal.goal.fingers.Finger_2 = current_traj.points[traj_stage].positions[i++];
            goal.goal.fingers.Finger_3 = current_traj.points[traj_stage].positions[i++];
            driver_fingers.sendGoal(goal.goal, boost::bind(&JointTrajectoryExecuter::driver_fingers_done_callback, this, _1, _2));
        }
    }

    /**
     * Receive an instruction/goal (a trajectory)
     * @param gh
     */
    void goal_callback(GoalHandle gh) {
        // Ensures that the joints in the goal match the joints we are commanding.
        ROS_DEBUG("Received goal: goal_callback");
        ROS_INFO("Received goal: goal_callback");
        if (setsEqual(joint_names, gh.getGoal()->trajectory.joint_names)) {
            active_joints = joint_names;
        } else if (setsEqual(arm_joint_names, gh.getGoal()->trajectory.joint_names)) {
            active_joints = arm_joint_names;
        } else if (setsEqual(finger_joint_names, gh.getGoal()->trajectory.joint_names)) {
            active_joints = finger_joint_names;
        } else {
            ROS_ERROR("Joints on incoming goal don't match any joint combinations");
            gh.setRejected();
            return;
        }

        // Cancel the currently active goal
        if (has_goal) {
            ROS_INFO("HAS ACTIVE");
            ROS_INFO("Received new goal, canceling current goal");
            // Mark the current goal as canceled
            goal.setCanceled();
            has_goal = false;
        }

        // Accept
        gh.setAccepted();
        has_goal = true;
        goal = gh;
        current_traj = goal.getGoal()->trajectory;
        traj_stage = 0;
        executed_stage = -1;

        ROS_INFO("Accepted goal");

        // Process
        execute();
    }

    /**
     * Receive a cancel trajectory instruction
     * @param gh
     */
    void cancel_callback(GoalHandle gh) {
        ROS_INFO("Received action cancel request");
        if (goal == gh) {
            // Marks the current goal as canceled.
            goal.setCanceled();
            has_goal = false;
        }
    }

    /**
     * Listen to joint_states
     * @param msg
     */
    void state_callback(const sensor_msgs::JointState &msg) {
        ROS_DEBUG("Checking controller state feedback");

        // Check this message contains all the joints we need
        if (!subset(joint_names, msg.name))
            return;

        // If we have a goal, see if we're almost at the next waypoint
        if (has_goal && atPose(msg, current_traj.points[traj_stage])) {
            ROS_INFO("Almost at waypoint!");
            ++traj_stage;
            execute();
        }
    }

    /**
     * Increment current state and check for goal
     */
    void step() {
        if (++traj_stage >= current_traj.points.size()) {
            ROS_INFO("Trajectory succeeded");
            has_goal = false;
            goal.setSucceeded();
            ROS_INFO("Finished goal.setSucceeded()");
        } else {
            execute();
        }
    }

    /**
     * Wait for JACO to move arm. This callback will be called when the action
     * is complete.
     * @param state
     * @param result
     */
    void driver_arm_done_callback(const actionlib::SimpleClientGoalState& state, const boost::shared_ptr<const jaco_driver::ArmJointAnglesResult_<std::allocator<void> > >& result) {
        ROS_INFO("Received DONE from jaco_driver arm");
        step();
    }

    /**
     * Wait for JACO to move fingers. This callback will be called when the 
     * action is complete.
     * @param state
     * @param result
     */
    void driver_fingers_done_callback(const actionlib::SimpleClientGoalState& state, const boost::shared_ptr<const jaco_driver::SetFingersPositionResult_<std::allocator<void> > >& result) {
        ROS_INFO("Received DONE from jaco_driver fingers");
        // Only execute on finger callback if the arm is not running.
        if (active_joints == finger_joint_names) {
            step();
        }
    }

};

int main(int argc, char** argv) {
    init(argc, argv, "jsim_joint_trajectory_action");
    NodeHandle node; //("~");
    JointTrajectoryExecuter jte(node);

    spin();

    return 0;
}