// JACO state forwarder node
// This node converts output from the ROS JACO package into the standard 
// ROS joint_states topic, including adding finger angles
//
// Ian Craig minicraig@gmail.com
// 03-11-2013

#include "ros/ros.h"
#include "sensor_msgs/JointState.h"
#include <jaco_driver/FingerPosition.h>

ros::Publisher pub;
float f1 = 0, f2 = 0, f3 = 0;

#define PI 3.1415926535897931

void jointStateCallback(sensor_msgs::JointState js) {
    // Add timestamp to message (so robot_state_publisher does not ignore it)
    js.header.stamp = ros::Time::now();
    // Add Finger angles to states
    js.name.resize(9);
    js.name[6] = "finger_joint_1";
    js.name[7] = "finger_joint_2";
    js.name[8] = "finger_joint_3";
    js.position.resize(9);
    js.position[6] = f1;
    js.position[7] = f2;
    js.position[8] = f3;
    js.velocity.resize(0);
    js.effort.resize(0);
    // Re-publish into the main joint_states topic
    pub.publish(js);
}

void fingerPositionCallback(const jaco_driver::FingerPosition fp) {
    // Save finger angles in radians
    f1 = fp.Finger_1 / 180 * PI;
    f2 = fp.Finger_2 / 180 * PI;
    f3 = fp.Finger_3 / 180 * PI;
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "state_forwarder");
    ros::NodeHandle n;

    // Advertise publisher
    pub = n.advertise<sensor_msgs::JointState>("joint_states", 1000);

    // Subscribe to the jaco messages
    ros::Subscriber jsub = n.subscribe("jaco/joint_state", 1000, jointStateCallback);
    ros::Subscriber fsub = n.subscribe("jaco/finger_position", 1000, fingerPositionCallback);

    // Spin on subscription
    ros::spin();

    return 0;
}